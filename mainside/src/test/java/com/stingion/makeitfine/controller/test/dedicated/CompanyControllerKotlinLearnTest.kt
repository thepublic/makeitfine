/*
 * Created under not commercial project "Make it fine"
 *
 * Copyright 2017-2021
 */

package com.stingion.makeitfine.controller.test.dedicated

import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import com.stingion.makeitfine.controller.CompanyController
import com.stingion.makeitfine.data.model.Company
import com.stingion.makeitfine.data.service.model.CompanyService
import com.stingion.makeitfine.testconfiguration.UnitTest
import helpful.helpful
import helpful.helpfulOther
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.Description
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import java.util.stream.Collectors
import kotlin.random.Random
import helpful.helpfulThird as third

@ExtendWith(SpringExtension::class)
@WebMvcTest(CompanyController::class)
@UnitTest
@Description("Temp class for learning kotlin")
class CompanyControllerKotlinLearnTest {

    companion object {
        val companies = listOf(
            Company(1, "company1", "Some desc 1"),
            Company(2, "company2", "Some desc 2"),
            Company(3, "company3", null)
        )
    }

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var companyService: CompanyService

    @BeforeEach
    fun `before each`() {
        `when`(companyService.findAll()).thenReturn(companies)
        `when`(companyService.findById(1)).thenReturn(companies[0])
        `when`(companyService.findById(2)).thenReturn(companies[1])
        `when`(companyService.findById(3)).thenReturn(companies[2])
    }

    fun getValue(companyUrlExtension: String) = mockMvc
        .perform(MockMvcRequestBuilders.get("/company${companyUrlExtension}"))
        .andDo(MockMvcResultHandlers.print())
        .andReturn()

    val prettyJson = { s: String? ->
        val gson = GsonBuilder().setPrettyPrinting().create()
        val je = JsonParser.parseString(s)
        gson.toJson(je)
    }

    val printlnPrettyJson: (String?) -> Unit = { s: String? -> println(prettyJson(s!!)) }

    @Test
    fun `check printlnPrettyJson`() {
        helpful()
        helpfulOther()
        third()

        assertDoesNotThrow { printlnPrettyJson("{\"a\":1234}") }
        assertThrows<NullPointerException> { printlnPrettyJson(null) }
    }

    @Test
    fun `list and map get and set`() {
        val list = mutableListOf<Int?>()

        assertThrows<IndexOutOfBoundsException> { list[0] = 3 }
        assertThrows<IndexOutOfBoundsException> { companies[3] }
        assertDoesNotThrow { list.add(1) }
        assertDoesNotThrow { list[0] = 7 }
        assertDoesNotThrow { companies[0] }

        val map = mutableMapOf<String, Double>()

        assertDoesNotThrow { map["1"] = 3.3 }
        assertDoesNotThrow { map["222"] }
        assertDoesNotThrow { map.put("2", 3.3) }
        assertDoesNotThrow { map.get("1") }

        assertTrue(map.size > 1)
        map["1"]?.let { e -> println("there is so key with value = $e") }
        map["null"]?.let { e -> println("there is so key with value = $e") }
    }

    @Test
    fun `check NPE`() {
        val company: Company? = companyService.findById(Int.MIN_VALUE);

        assertThrows<NullPointerException> { company!!.id }
        assertDoesNotThrow { company?.id }
        assertThrows<NullPointerException> { printlnPrettyJson(company?.name) }
    }

    @Test
    fun `infix fun`() {
        assertEquals(2, this showInf 2.2)
        assertNotEquals(2, this showInf 12.2)

        assertEquals(companies[1]?.id to "2", Pair(2, "2"))
        assertDoesNotThrow { assertEquals(companies[2]!!.id to "3", Pair(3, "3")) }

        assertDoesNotThrow { assertEquals(companies[2]!!.description to "4", Pair(null, "4")) }
        assertThrows<NullPointerException> { assertEquals(companies[2]!!.description!! to "4", Pair(null, "4")) }
    }

    private infix fun showInf(d: Double): Int {
        return d.toInt()
    }

    @Test
    fun `fun by default`() {
        assertDoesNotThrow { println(show(d = 2.2, sign = "!")) }
        assertDoesNotThrow { println(show(d = 2.2)) }
        assertDoesNotThrow { println(show(2, 3.2, "?")) }
        assertDoesNotThrow { show { println("abc") } }
        assertDoesNotThrow { showNext2("") { a -> println("=$a=") } }
    }

    fun show(other: Int = 1, d: Double, sign: String = "<>"): String {
        return "$sign$d$sign + i"
    }

    fun show(c: () -> Unit) = c.invoke()

    fun showNext(c: (a: String) -> Unit): Unit = c.invoke("111")

    fun showNext2(s: String = "!!!", c: (a: String) -> Unit): Unit = c.invoke(s)

    @Test
    fun namedParam() {
        manyParam(name = "Sir", surName = "Any", age = 5)
        manyParam(age = 5, name = "Sir")
        val defName: String = "Mo"
        manyParam(age = 5, name = defName)
        manyParam(age = 5, name = defName, p = *arrayOf("a1", "b1", "c1"))

        one(1, 2, 3, 4, 5)?.let { e -> println(e) }
    }

    fun one(vararg p: Int) = p.map { e -> e.toString() }.reduce { e1, e2 -> "$e1 $e2" }

    fun manyParam(name: String, surName: String = "Tomson", age: Int, vararg p: String) = println(
        "$name $surName $age ${
            java.util.Arrays
                .toString(p)
        }"
    )

    @Test
    fun `get company by wrong id format (fail)`() {
        val value = getValue("")

        val content = value.response.contentAsString
        printlnPrettyJson(content)

        val id = if (companyService.findById(Random.nextInt(3) + 1).id!! > 1) 1 else 2
        println("if id=$id")

        println("check third element id (${companies[2].id})")
        assertThat(companies.stream().map { e -> e.id }.collect(Collectors.toList())).contains(3)

        assertNotNull(content)
    }

    @Test
    fun `inside funs`() {
        assertDoesNotThrow { insideHave(1) }
        assertThrows<NullPointerException> { insideHave(null) }
    }

    fun insideHave(i: Int?) {
        var k1 = 1;
        val k2 = 1;
        fun inside(j: Int?) {
            k1 = 2
            println("$k1 $k2")
            i!!.toString()
        }
        inside(i)
        println(k1)
    }

    @Test
    fun `small check General types`() {
        assertDoesNotThrow {
            val s: ArrayList<S<String>>? = null
            val obj: List<S<Any>>? = s
            obj.toString()
        }

        assertDoesNotThrow {
            var s: MutableList<SI<Number>> = mutableListOf()
            s.add(SI<Int>() as SI<Number>)
            s[0].pr(2)
            s[0].pr(2.5)
        }

        `generic funs`(3, 3.5)
    }

    fun <T, K> `generic funs`(i: T, k: K) {
        println("$i <> $k")
    }

    class S<out T> {
    }

    class SI<in T> {
        fun pr(i: T) {
            println(i.toString())
        }
    }

    @Test
    fun `tailrec`() {
        assertEquals((120).toLong(), factorial(5))
    }

    tailrec fun factorial(n: Int): Long = if (n < 2) (1).toLong() else n * factorial(n - 1)

    @Test
    fun `inline funcs`() {

    }
}
